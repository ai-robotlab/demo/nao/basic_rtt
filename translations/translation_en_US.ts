<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Stijn</source>
            <comment>Text</comment>
            <translation type="obsolete">Stijn</translation>
        </message>
        <message>
            <source>Hello world</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello world</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Get Localized Text</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You touched my right foot</source>
            <comment>Text</comment>
            <translation type="unfinished">You touched my right foot</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Get Localized Text (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You touched my left foot</source>
            <comment>Text</comment>
            <translation type="unfinished">You touched my left foot</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Get Localized Text (2)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You touched my head</source>
            <comment>Text</comment>
            <translation type="unfinished">You touched my head</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Get Localized Text (3)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You touched my right hand</source>
            <comment>Text</comment>
            <translation type="unfinished">You touched my right hand</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Get Localized Text (4)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You touched my left hand</source>
            <comment>Text</comment>
            <translation type="unfinished">You touched my left hand</translation>
        </message>
    </context>
</TS>
